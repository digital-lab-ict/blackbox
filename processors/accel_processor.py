import math
import time
import logging
import base_processor

class AccelDataProcessor(base_processor.BaseDataProcessor):

  def __init__(self, name, telemetry_streams, persistence, server, processor_config):
    base_processor.BaseDataProcessor.__init__(self, name, telemetry_streams, persistence, server, processor_config)
    self._xy_limit_g = processor_config["xy_limit_g"]
    self._xy_limit_duration = processor_config["xy_limit_duration"]
    self._xy_interval_duration = processor_config["xy_interval_duration"]
    self._xy_limit_last = 0.0
    self._xy_event_last = 0.0
    self._z_limit_g = processor_config["z_limit_g"]
    self._z_limit_duration = processor_config["z_limit_duration"]
    self._z_interval_duration = processor_config["z_interval_duration"]
    self._z_limit_last = 0.0
    self._z_event_last = 0.0

  def process(self):
    for stream in self._telemetry_streams:
      n = 0
      s = {"accel": {"x": 0.0, "y": 0.0, "z": 0.0}, "gyro": {"x": 0.0, "y": 0.0, "z": 0.0}, "comp": {"x": 0.0, "y": 0.0, "z": 0.0},"temp": 0.0}
      while len(stream) > 0 and time.time() - self._processor_last >= self._processor_timeout:
        t = stream.get()
        data = t["data"]
        ts = t["ts"]
        s["accel"]["x"] += data["accel"]["x"]
        s["accel"]["y"] += data["accel"]["y"]
        s["accel"]["z"] += data["accel"]["z"]
        s["gyro"]["x"] += data["gyro"]["x"]
        s["gyro"]["y"] += data["gyro"]["y"]
        s["gyro"]["z"] += data["gyro"]["z"]
        s["comp"]["x"] += data["comp"]["x"]
        s["comp"]["y"] += data["comp"]["y"]
        s["comp"]["z"] += data["comp"]["z"]
        s["temp"] += data["temp"]
        mag_xy = math.sqrt(math.pow(data["accel"]["x"],2) + math.pow(data["accel"]["y"],2))
	mag_z = data["accel"]["z"]
        #logging.info("xy: " + str(mag_xy))
        if mag_xy > self._xy_limit_g:
          if self._xy_limit_last == 0.0:
            self._xy_limit_last = ts
          elif t["ts"] - self._xy_limit_last > self._xy_limit_duration and time.time() - self._xy_event_last > self._xy_interval_duration:
            self.create_event("collision", t["ts"], {"magnitudo": mag_xy})
            self._xy_limit_last = 0.0
            self._xy_event_last = time.time()
        else:
          self._xy_limit_last = 0.0

        if mag_z > self._z_limit_g:
          if self._z_limit_last == 0.0:
            self._z_limit_last = ts
          elif ts - self._z_limit_last > self._z_limit_duration and time.time() - self._z_event_last > self._z_interval_duration:
            self.create_event("tow", t["ts"], {"magnitudo": mag_z})
            self._z_limit_last = 0.0
            self._z_event_last = time.time()
        else:
          self._z_limit_last = 0.0

        n += 1

      if n > 0:
        nt = {}
        t["ts"] = t["ts"]
        t["sensor"] = t["sensor"]
        td = s #init
	td["accel"]["x"] = s["accel"]["x"] / n
        td["accel"]["y"] = s["accel"]["y"] / n
        td["accel"]["z"] = s["accel"]["z"] / n
        td["gyro"]["x"] = s["gyro"]["x"] / n
        td["gyro"]["y"] = s["gyro"]["y"] / n
        td["gyro"]["z"] = s["gyro"]["z"] / n
        td["comp"]["x"] = s["comp"]["x"] / n
        td["comp"]["y"] = s["comp"]["y"] / n
        td["comp"]["z"] = s["comp"]["z"] / n
        td["temp"] = s["temp"] / n
        t["data"] = td
        self._persistence.save(t)
	self._processor_last = time.time()
        logging.info("processed: " + str(t["sensor"]) + " n: " + str(n))

