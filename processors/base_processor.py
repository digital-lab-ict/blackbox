import sys
import threading
import time
import logging
import api

class BaseDataProcessor(threading.Thread):

  def __init__(self, name, telemetry_streams, persistence, publish_server, processor_config):
    threading.Thread.__init__(self)
    self._name = name
    self._telemetry_streams = telemetry_streams
    self._persistence = persistence
    self._server_stub = publish_server
    self._processor_timeout = processor_config["timeout"]
    self._processor_last = 0
    self.go = True

  def run(self):
    while self.go:
      if time.time() - self._processor_last >= self._processor_timeout:
        self.process()
        self.publish()
        self._processor_last = time.time()
      time.sleep(max(0,self._processor_timeout - (time.time() - self._processor_last)))

  def stop(self):
    self.go = False
    self.join()

  def process(self):
    for stream in self._telemetry_streams:
      while True:
        data = stream.get() 
        if data == None:
          break
        self._persistence.save(data)

  def publish(self):
    item = self._persistence.peek_unpublished()
    if item:
      item_as_dict = item.as_dict()
      #logging.info("publishing: " + str(item_as_dict["data"]["sensor"]))
      try:
        ts = time.time()
        self._server_stub.publish("telemetry", item_as_dict)
	logging.info("published: " + str(item_as_dict["data"]["sensor"]) + " time: " + str(time.time() - ts))
        item.set_pub()
      except:
        logging.info("publishing error: " + str(sys.exc_info()[0]))
        
  def create_event(self, event_type, ts, data):
    logging.info("event: " + event_type)
    event_data = {"category": event_type, "ts": ts, "data": data}
    try:
      self._server_stub.publish("event", event_data)
    except:
      logging.info("publishing error: " + str(sys.exc_info()[0]))

