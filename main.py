from flask import Flask, render_template, request
import pigpio

PIN_LED_RED = 16 
PIN_LED_WHITE = 20
PIN_LED_GREEN = 21

app = Flask(__name__) 

gpio = pigpio.pi('localhost')
gpio.set_mode(PIN_LED_RED, pigpio.OUTPUT)
gpio.set_mode(PIN_LED_WHITE, pigpio.OUTPUT)
gpio.set_mode(PIN_LED_GREEN, pigpio.OUTPUT)

led_map = {"red":PIN_LED_RED, "white": PIN_LED_WHITE, "green":PIN_LED_GREEN}
status_map = {"on": 1, "off": 0}

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/led/<id>")
def led(id, methods=["GET"]):
    status = status_map.get(request.args.get("status"), 0)
    gpio.write(led_map.get(id, 0), status)

    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)



