from collections import deque

class Events:
  class Event:
    def __init__(self, typ, data, ts):
      self._typ = typ
      self._data = data
      self._ts = ts

    def get_type(self):
      return self._typ

    def get_data(self):
      return self._data
 
    def get_ts(self):
      return self._ts

    def as_dict(self):
      return {"type": self._typ, "ts": self._ts, "data": self._data}

  def __init__(self, maxlen=100):
    self._events = deque(maxlen=maxlen)

  def save(self, event):
    self._events.append(event) 
