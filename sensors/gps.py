import logging

import sensor

class GPS(sensor.Sensor):
  def __init__(self, name):
    sensor.Sensor.__init__(self, name)
    self.poll_freq = 1.0

  def read(self):
    return {"position": {"lat": 45.42434, "lon": 9.256116}, "speed": "5.4"}
