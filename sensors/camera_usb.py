import logging
import base64
import PIL.Image
from pygame.locals import *
import pygame.camera

import sensor

pygame.init()
pygame.camera.init()

class Camera(sensor.Sensor):
  def __init__(self, name):
    sensor.Sensor.__init__(self, name)
    self._poll_freq = 1.0
    self._cam = pygame.camera.Camera("/dev/video0",(640,480))
    self._cam.start()

  def read(self):
    data = {}
    image = self._cam.get_image()
    image = pygame.image.tostring(image, "RGB")
    image = PIL.Image.frombytes("RGB", (640, 480), image)
    data["image"] = base64.b64encode(image.tobytes())
    return data


