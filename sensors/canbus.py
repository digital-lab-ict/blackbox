from obdython import Device, OBDPort, SENSORS
import logging

import time
import sys
import argparse
import csv
import signal
import sys

import sensor

BLUETOOTH_MAC="00:0D:18:00:00:01"
BLUETOOTH_CHANNEL=16

class CanBus(sensor.Sensor):
  def __init__(self, name, telemetry, sensor_config):
    sensor.Sensor.__init__(self, name, telemetry, sensor_config)
    try:
        device = Device(Device.types['bluetooth'], bluetooth_mac=BLUETOOTH_MAC, bluetooth_channel=BLUETOOTH_CHANNEL, timeout=60)
        self.port = OBDPort(device)
        time.sleep(0.1)
        self.port.connect()
        time.sleep(0.1)
        self.port.ready()
        logging.info("OBD-II version: " + self.port.get_elm_version())
        self.bt_connected = 1
    except:
        logging.error("bluetooth OBD-II reader not connected")
        self.bt_connected = 0

  def read(self):
    data = {}
    if self.bt_connected:
      data["fuel_level"] = self.port.sensor('fuel_level')[1]
      data["load"] =self.port.sensor('load')[1]
      data["rpm"] = self.port.sensor('rpm')[1]
      data["throttle_pos"] = self.port.sensor('throttle_pos')[1]
      data["speed"] = self.port.sensor('speed')[1]
      data["temp"] = self.port.sensor('temp')[1]
      data["intake_air_temp"] = self.port.sensor('intake_air_temp')[1]
    d = sensor.Sensor.read(self)
    d["data"] = data
    return d
