import gps
import logging
import threading
import time

import sensor

class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    self.session = gps.gps(mode=gps.WATCH_ENABLE)
    self.current_value = None

  def get_current_value(self):
    return self.current_value

  def run(self):
    try:
      while True:
        self.current_value = self.session.next()
        time.sleep(0.2) # tune this, you might not get values that quickly
    except StopIteration:
      pass

class GPS(sensor.Sensor):
  def __init__(self, name, telemetry, sensor_config):
    sensor.Sensor.__init__(self, name, telemetry, sensor_config)
    self.gpsp = GpsPoller()
    self.gpsp.start()

  def read(self):
    dict = {}
    dict['time'] = ''
    dict['speed'] = 0
    dict['lat'] = 0
    dict['lon'] = 0
    dict['alt'] = 0

    dict['time'] = self.gpsp.session.utc
    dict['speed'] = str(self.gpsp.session.fix.speed * gps.MPS_TO_KPH)
    dict['lat'] = str(self.gpsp.session.fix.latitude)
    dict['lon'] = str(self.gpsp.session.fix.longitude)
    dict['alt'] = str(self.gpsp.session.fix.altitude)
    
    #logging.info({"position": {"lat": dict['lat'], "lon": dict['lon'], "alt": dict['alt']}, "speed": dict['speed'], "gps_time": dict['time']})
    data = {}
    if dict['time'] != '':  
      data["position"] = {"lat": dict['lat'], "lon": dict['lon'], "alt": dict['alt']}
      data["speed"] = dict['speed']
      data["gps_time"] = dict['time']
    d = sensor.Sensor.read(self)
    d["data"] = data
    return d
