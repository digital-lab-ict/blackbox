import time
import threading
import logging

class Sensor:
  class Reader(threading.Thread):
    def __init__(self, sensor):
      threading.Thread.__init__(self)
      self.sensor = sensor
      self.go = True

    def run(self):
      while self.go:
        if time.time() - self.sensor.read_last >= self.sensor.read_timeout:
          self.sensor.save_to_telemetry()
          self.sensor.read_last = time.time()
        time.sleep(max(0, self.sensor.read_timeout - (time.time() - self.sensor.read_last))) 


  def __init__(self, name, telemetry_stream, sensor_config):
    self._name = name
    self.stream = telemetry_stream
    self.read_last = 0
    self.read_timeout = float(sensor_config["timeout"])
    self._reader = self.Reader(self)

  def start(self):
    self._reader.start()

  def stop(self):
    self._reader.go = False
    self._reader.join()

  def get_name(self):
    return self._name

  def read(self):
    return {"sensor": self._name, "ts": time.time(), "data": None}

  def save_to_telemetry(self):
    #logging.info("sensor.save: " + self.get_name())
    self.stream.append(self.read())
