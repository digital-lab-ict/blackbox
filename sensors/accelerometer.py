import time
import sensor
import MPU6050
import random
import logging
import math

class Accelerometer(sensor.Sensor):

  def __init__(self, name, telemetry, sensor_config):
    sensor.Sensor.__init__(self, name, telemetry, sensor_config)
    self._imu = MPU6050.MPU6050(0x68)
    self._imu.set_accel_range(self._imu.ACCEL_RANGE_4G)
    self._imu.set_gyro_range(self._imu.GYRO_RANGE_500DEG)
    self._comp = [0.0, 0.0, 0.0]
    #logging.info("accel range: " + str(self._imudd.read_accel_range()) + " gyro range: " + str(self._imu.read_gyro_range()))

  def read(self):
    data = {}
    data["temp"] = self._imu.get_temp()
    data["accel"] = self._imu.get_accel_data(True)
    data["gyro"] = self._imu.get_gyro_data()
    data["comp"] = self.complementary_filter([data["accel"]["x"], data["accel"]["y"],data["accel"]["z"]], [data["gyro"]["x"], data["gyro"]["y"], data["gyro"]["z"]])
    d = sensor.Sensor.read(self)
    d["data"] = data
    return d

  def complementary_filter(self, adata, gdata):

    self._comp[0] += gdata[0] * self.read_timeout
    self._comp[1] -= gdata[1] * self.read_timeout
    self._comp[2] += gdata[2] * self.read_timeout

    avect = abs(adata[0]) + abs(adata[1]) + abs(adata[2])
    if avect > 0.5 and avect < 2.0:
      pitchAcc = math.atan2(adata[1], adata[2]) * 180 / math.pi
      self._comp[0] = self._comp[0] * 0.98 + pitchAcc * 0.02

      rollAcc = math.atan2(adata[0], adata[2]) * 180 / math.pi
      self._comp[1] = self._comp[1] * 0.98 + rollAcc * 0.02

    return {"x": self._comp[0], "y": self._comp[1], "z": self._comp[2]}   
