import logging
import base64
import picamera
import io

import sensor

class Camera(sensor.Sensor):
  def __init__(self, name, telemetry, sensor_config):
    sensor.Sensor.__init__(self, name, telemetry, sensor_config)
    self._camera = picamera.PiCamera()
    self._camera.resolution = (640, 480)
    self._camera.framerate = 30
    self._jpeg_quality = int(sensor_config["jpeg_quality"])
    self.out_jpeg = io.BytesIO()
    self.grab_start()

  def grab_start(self):
    logging.info("jpeg_quality: " + str(self._jpeg_quality))
    camera_port_0, output_port_0 = self._camera._get_ports(True, 0)
    self.jpeg_encoder = self._camera._get_image_encoder(camera_port_0, output_port_0, 'jpeg', None, quality=self._jpeg_quality)

    with self._camera._encoders_lock:
      self._camera._encoders[0] = self.jpeg_encoder

  def grab_one(self):
    self.out_jpeg.seek(0)

    self.jpeg_encoder.start(self.out_jpeg)

    if not self.jpeg_encoder.wait(10):
      raise picamera.PiCameraError('Timed out')

  def grab_stop(self):
    with self._camera._encoders_lock:
      del self._camera._encoders[0]

    self.jpeg_encoder.close()

  def get_image_jpeg(self):
    return self.out_jpeg.getvalue()

  def read(self):
    data = {}
    self.grab_one()
    image = self.get_image_jpeg()
    data["image"] = base64.b64encode(image)
    d = sensor.Sensor.read(self)
    d["data"] = data
    logging.info("image_size: " + str(len(data["image"])))
    return d

  def stop(self):
    sensor.Sensor.stop(self)
    self.grab_stop()

