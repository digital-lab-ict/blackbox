import time
import threading

from collections import deque

class Persistence:

  class Store:
    class Item:
      def __init__(self, data):
        self._ts = time.time()
        self._pub = False
        self._data = data

      def pub(self):
        return self._pub

      def set_pub(self):
        self._pub = True

      def as_dict(self):
        return {"ts": self._ts, "data": self._data}

    def __init__(self):
      self._store = deque(maxlen=1) #in ram
      self.lock = threading.RLock()
    
    def save(self, data):
      self.lock.acquire()
      self._store.append(self.Item(data))
      self.lock.release()

    def peek_unpublished(self):
      item = None
      self.lock.acquire()
      for i in self._store:
        if i.pub() == False:
           item = i
           break
      self.lock.release()
      return item

    def set_published(self, item):
      self.lock.acquire()
      item.set_pub()
      self.lock.release()
      
  def __init__(self):
    self._stores = {}
    self._lock = threading.RLock()

  def get_store(self, name):
    self._lock.acquire()
    store = self._stores.get(name)
    if store is None:
      store = self.Store()
      self._stores[name] = store
    self._lock.release()
    return store
