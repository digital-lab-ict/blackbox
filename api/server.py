import httplib, urllib
import json
import logging

class ServerStub:

  def __init__(self, host, client_id):
    self._client_id = client_id
    self._host = host
    #self._conn = httplib.HTTPConnection(self._host, timeout=3.0)

  def publish(self, what, data):
    conn = httplib.HTTPConnection(self._host, timeout=3)
    body = json.dumps(data)
    #logging.info("publishing: " + str(body))
    conn.request("POST", '/api/vehicle/' + self._client_id + '/' + what, body)
    response = conn.getresponse()
    response.read()
    if response.status != 200:
      logging.error("unable to publish data")
      raise httplib.HTTPException()
