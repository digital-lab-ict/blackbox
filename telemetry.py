import threading
from collections import deque

class Telemetry:

  class Stream:
    def __init__(self, name, maxlen=None):
      self._name = name
      self._data = deque(maxlen=maxlen)
      self._lock = threading.RLock()

    def __len__(self):
      return len(self._data)

    def append(self, data):
      self._lock.acquire()
      self._data.append(data)
      self._lock.release()

    def get(self):
      self._lock.acquire()
      data = self._data.popleft() if len(self._data) else None
      self._lock.release()
      return data 

  def __init__(self):
    self._streams = {}

  def add_stream(self, name):
    self._streams[name] = self.Stream(name)
    return self._streams[name]

  def get_stream(self, name):
    return self._streams.get(name)
