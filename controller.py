import threading
import time
import logging
import yaml
import importlib

import telemetry
import persistence
import event

import api.server

import processors.base_processor
import processors.accel_processor

logging.basicConfig(level=logging.DEBUG)

class Controller:

  def __init__(self):
    #threading.Thread.__init__(self)
    self._config = yaml.load(file("controller.yaml", "r"))
    logging.info(self._config)
    self._sensors = {}
    self._processors = {}
    self._client_id = self._config["client_id"]
    self._telemetry = telemetry.Telemetry()
    self._persistence = persistence.Persistence()
    self._server_stub = api.server.ServerStub(self._config["server_url"], self._client_id)
    for s in self._config["sensors"]:
      module_ = importlib.import_module(s["module"])
      class_ = getattr(module_, s["class"])
      self.register_sensor(class_, s["name"], s)
    
    for p in self._config["processors"]:
      module_ = importlib.import_module(p["module"])
      class_ = getattr(module_, p["class"])
      self.register_processor(class_, p["name"], p["streams"], p)


  def register_sensor(self, sensor_class, sensor_name, sensor_config):
    sensor_telemetry = self._telemetry.add_stream(sensor_name)
    sensor = sensor_class(sensor_name, sensor_telemetry, sensor_config)
    self._sensors[sensor_name] = sensor

  def register_processor(self, processor_class, processor_name, stream_names, processor_config):
    telemetry_streams = [self._telemetry.get_stream(n) for n in stream_names]
    processor = processor_class(processor_name, telemetry_streams, self._persistence.get_store(processor_name), self._server_stub, processor_config)
    self._processors[processor_name] = processor

  def start(self):
    for s in self._sensors.values():
      s.start()
    for p in self._processors.values():
      p.start()
    
    try:
      self.run()
    except KeyboardInterrupt:
      for s in self._sensors.values():
        s.stop()
      for p in self._processors.values():
        p.stop()

  def run(self):
    while True:
      time.sleep(1.0)
      #self.publish()

  def publish(self):
    item = self._persistence.peek_unpublished()
    if item:
      item_as_dict = item.as_dict()
      logging.info("publishing: " + str(item_as_dict["data"]["sensor"]))
      self._server_stub.publish("telemetry", item_as_dict)
      item.set_pub()


def main():
  controller = Controller()
  controller.start()

main()




